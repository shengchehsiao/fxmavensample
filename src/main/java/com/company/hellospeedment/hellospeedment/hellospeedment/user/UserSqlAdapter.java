package com.company.hellospeedment.hellospeedment.hellospeedment.user;

import com.company.hellospeedment.hellospeedment.hellospeedment.user.generated.GeneratedUserSqlAdapter;

/**
 * The SqlAdapter for every {@link
 * com.company.hellospeedment.hellospeedment.hellospeedment.user.User} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public class UserSqlAdapter extends GeneratedUserSqlAdapter {}