package com.company.hellospeedment.hellospeedment.hellospeedment.user;

import com.company.hellospeedment.hellospeedment.hellospeedment.user.generated.GeneratedUser;

/**
 * The main interface for entities of the {@code user}-table in the database.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public interface User extends GeneratedUser {}