package com.company.hellospeedment.hellospeedment.hellospeedment.user;

import com.company.hellospeedment.hellospeedment.hellospeedment.user.generated.GeneratedUserManager;

/**
 * The main interface for the manager of every {@link
 * com.company.hellospeedment.hellospeedment.hellospeedment.user.User} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public interface UserManager extends GeneratedUserManager {}