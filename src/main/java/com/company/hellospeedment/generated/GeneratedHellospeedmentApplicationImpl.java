package com.company.hellospeedment.generated;

import com.speedment.common.annotation.GeneratedCode;
import com.speedment.runtime.core.internal.AbstractSpeedment;

/**
 * The generated {@link
 * com.speedment.runtime.core.internal.AbstractApplicationBuilder}
 * implementation class for the {@link com.speedment.runtime.config.Project}
 * named hellospeedment.
 * <p>
 * This file has been automatically generated by Speedment. Any changes made to
 * it will be overwritten.
 * 
 * @author Speedment
 */
@GeneratedCode("Speedment")
public class GeneratedHellospeedmentApplicationImpl 
extends AbstractSpeedment 
implements GeneratedHellospeedmentApplication {}