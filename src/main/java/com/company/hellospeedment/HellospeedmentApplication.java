package com.company.hellospeedment;

import com.company.hellospeedment.generated.GeneratedHellospeedmentApplication;

/**
 * An {@link com.speedment.runtime.core.ApplicationBuilder} interface for the
 * {@link com.speedment.runtime.config.Project} named hellospeedment.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public interface HellospeedmentApplication extends GeneratedHellospeedmentApplication {}