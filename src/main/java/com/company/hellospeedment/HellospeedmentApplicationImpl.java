package com.company.hellospeedment;

import com.company.hellospeedment.generated.GeneratedHellospeedmentApplicationImpl;

/**
 * The default {@link com.speedment.runtime.core.Speedment} implementation class
 * for the {@link com.speedment.runtime.config.Project} named hellospeedment.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public final class HellospeedmentApplicationImpl 
extends GeneratedHellospeedmentApplicationImpl 
implements HellospeedmentApplication {}