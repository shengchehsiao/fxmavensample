import com.company.hellospeedment.HellospeedmentApplication;
import com.company.hellospeedment.HellospeedmentApplicationBuilder;
import com.company.hellospeedment.hellospeedment.hellospeedment.user.User;
import com.company.hellospeedment.hellospeedment.hellospeedment.user.UserImpl;
import com.company.hellospeedment.hellospeedment.hellospeedment.user.UserManager;
import com.speedment.runtime.core.ApplicationBuilder;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class MyDBUtils {

    protected static HellospeedmentApplication getAppliaction() {
        return new HellospeedmentApplicationBuilder()
                .withUsername("root")
                .withPassword("adminpwd")
                .withLogging(ApplicationBuilder.LogType.PERSIST)
                .withLogging(ApplicationBuilder.LogType.STREAM)
                .withLogging(ApplicationBuilder.LogType.UPDATE)
                .build();
    }

    protected static UserManager getUserManager() {
        return getAppliaction().getOrThrow(UserManager.class);
    }

    /*
    public static User addUser(User user) {
        return getUserManager().persist(user);
    }
    */

    public static void addUser(User user) {
        Optional<User> optional = getUserManager().stream().filter(User.NAME.equal(user.getName()))
                .findAny();
        if (optional.isPresent()) {
            //update
            user.setId(optional.get().getId());
            getUserManager().update(user);
            System.out.println("Update");

        } else {
            //persist
            getUserManager().persist(user);
            System.out.println("Persist");
        }

    }

    public static List<String> getAllNames() {
        List<String> strings = new ArrayList();
        getUserManager().stream().forEach(e -> {
            strings.add(e.getName());
        });
        return strings;
    }

    public static void updateUser(User user) {
        getUserManager().update(user);
    }

    public static void listAllUser() {
        getUserManager().stream().forEach(System.out::println);
    }

    public static ObservableList<User> getUsers() {
        //return FXCollections.observableArrayList(getUserManager().stream().collect(Collectors.toList()));

        /*
        List<User> userList = getUserManager().stream().collect(Collectors.toList());
        ObservableList<User> userObservableList
                 = FXCollections.observableList(userList);
        return userObservableList;
        */
        return FXCollections.observableArrayList(
                getUserManager().
                        stream().
                        collect(
                                Collectors.toList()));
    }


}
