import com.company.hellospeedment.hellospeedment.hellospeedment.user.User;
import com.company.hellospeedment.hellospeedment.hellospeedment.user.UserImpl;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class FX extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        VBox vBox = new VBox();
        vBox.setSpacing(5);
        vBox.setPadding(new Insets(10, 10, 30, 10));

        TableView tableView = new TableView();
        tableView.setEditable(true);
        tableView.setItems(MyDBUtils.getUsers());

        TableColumn tcid = new TableColumn("ID");

        tcid.setCellValueFactory(new PropertyValueFactory<User, Integer>("id"));


        TableColumn tcname = new TableColumn("NAME");
        tcname.setCellValueFactory(new PropertyValueFactory<User, String>("name"));
        tcname.setCellFactory(TextFieldTableCell.forTableColumn());
        tcname.setOnEditCommit(event -> {
            TableColumn.CellEditEvent e = (TableColumn.CellEditEvent) event;

            User user = (User) e.getRowValue();

            user.setName(e.getNewValue().toString());
            if (!MyDBUtils.getAllNames().contains(user.getName())) {
                MyDBUtils.updateUser(user);
            }

            tableView.setItems(MyDBUtils.getUsers());

        });


        TableColumn tcage = new TableColumn("AGE");
        tcage.setCellValueFactory(new PropertyValueFactory<User, Integer>("age"));


        tableView.getColumns().addAll(tcid, tcname, tcage);

        HBox hBox = new HBox();
        hBox.setPadding(new Insets(2, 2, 2, 2));
        TextField tfName = new TextField();
        TextField tfAge = new TextField();
        Button btnAdd = new Button("Add User");
        btnAdd.setOnAction(e -> {
            MyDBUtils.addUser(new UserImpl()
                    .setAge(Integer.parseInt(tfAge.getText()))
                    .setName(tfName.getText()));
            tableView.setItems(MyDBUtils.getUsers());
        });


        hBox.getChildren().addAll(tfName, tfAge, btnAdd);


        vBox.getChildren().addAll(tableView, hBox);


        Scene scene = new Scene(new Group(), 600, 400);
        scene.setRoot(vBox);
        primaryStage.setTitle("My First FX Maven Project");
        primaryStage.setScene(scene);
        setUserAgentStylesheet(STYLESHEET_MODENA);
        primaryStage.show();
    }
}
